// @flow

/**
 * The database used while running application.
 */
export let productionDatabase = {
  url: 'mysql-ait.stud.idi.ntnu.no',
  user: 'stineof',
  password: 'secret',
  database: 'stineof',
};
/**
 * The database used while running tests for the application.
 */
export let testDatabase = {
  url: 'mysql-ait.stud.idi.ntnu.no',
  user: 'stineof',
  password: 'secret',
  database: 'stineof',
};
